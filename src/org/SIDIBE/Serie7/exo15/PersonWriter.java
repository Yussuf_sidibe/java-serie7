package org.SIDIBE.Serie7.exo15;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.function.Function;

public class PersonWriter implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void write(List<Person> people, String fileName) {
		//************
		Function<Person,String> personToString = p->p.getLastName()+","+p.getFirstName()+","+p.getAge();
		//*********
		
		// d�finition d'un fichier
		File fichier = new File(fileName);

		// la d�finition du writer doit se faire ici
		// pour des raisons de visibilit�
		// BufferedWriter writer = null ;

		try (FileWriter writer = new FileWriter(fichier, true);
				BufferedWriter bfw = new BufferedWriter(writer);) {

			// ouverture d'un flux de sortie sur un fichier
			// a pour effet de cr�er le fichier
			String chaine = "";
			// �criture dans le fichier

			
			for( Person p : people){
				chaine = personToString.apply(p);
				bfw.write("\n");
				bfw.write(chaine);
				
			}
				
				System.out.println("Ecriture effectu�e avec succ�s !!!!!!!!!!");
				System.out.println("V�rifiez dans le contenu du fichier ");
			}
		catch (IOException e) {

			// affichage du message d'erreur et de la pile d'appel
			System.out.println("Erreur " + e.getMessage());
			e.printStackTrace();
		}
		
		} 
	}

