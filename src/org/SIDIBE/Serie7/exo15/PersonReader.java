package org.SIDIBE.Serie7.exo15;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PersonReader  implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public List<Person> read(String fileName){

		// ***********function qui prend une ligne et retourne une personne
		Function<String, Person> lineToPerson = ligne -> {
			String[] champs;
			Person p = new Person();
			champs = ligne.split(",");

			p.setLastName(champs[0]);
			p.setFirstName(champs[1]);
			p.setAge(Integer.parseInt(champs[2]));
			return p;
		};
		// ***function qui prend une chaine de caractere et retourne un stream
		// de Person
		Function<String, Stream<Person>> lineToStreamPerson = line -> (line.startsWith("#") || line.isEmpty())
				? Stream.empty()
				: Stream.of(new Person(line.split(",")[0], line.split(",")[1], Integer.parseInt(line.split(",")[2])));

		// *************

		List<Person> list = new ArrayList<>();

		try (FileReader fr = new FileReader(fileName); BufferedReader bf = new BufferedReader(fr);) {
			// ouverture d'un flux sur le flux de lecture du fichier
			Stream<Person> line = null;

			line = bf.lines().filter(l -> (!l.isEmpty() && !l.startsWith("#"))).map(lineToPerson);
			
			//**utilisation de flatmap
			Stream<Person> lineTestFlatMap = bf.lines().flatMap(lineToStreamPerson);

			list = line.collect(Collectors.toList());
			// list= lineTestFlatMap.collect(Collectors.toList());

			// quelques informations sur la lecture
		} catch (IOException | StringIndexOutOfBoundsException e) {

			// gestion de l'erreur
			System.out.println("Erreur " + e.getMessage());
			e.printStackTrace();
		}
		return list;
	}

}
