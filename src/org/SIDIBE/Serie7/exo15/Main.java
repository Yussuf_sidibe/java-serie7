package org.SIDIBE.Serie7.exo15;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		
		PersonReader pr = new PersonReader();
		List<Person> test = pr.read("files/test.txt");
		System.out.println(test);
		
		
		PersonWriter ecriture = new PersonWriter();
		
		List<Person> people = Arrays.asList(new Person("moi", "oui", 13), new Person("toi", "non", 12));
		ecriture.write(people, "files/test.txt");
		
		
	}

}
